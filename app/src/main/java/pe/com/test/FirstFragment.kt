package pe.com.test

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import pe.com.test.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private var binding: FragmentFirstBinding? = null
    var view_model: MyViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentFirstBinding.inflate(inflater, container, false)

        view_model = MyViewModel(requireActivity().application)

        view_model!!.data.observe(viewLifecycleOwner) { user ->
            binding?.textViewUser?.text = user!!.name
        }

        view_model!!.error.observe(viewLifecycleOwner) { error ->
            binding?.textViewUser?.text = ""
            Snackbar.make(binding!!.baseView, error, Snackbar.LENGTH_LONG).show()
        }

        binding?.buttonSearch?.setOnClickListener {
            view_model!!.data(binding!!.searchEditText.text.toString())
        }

        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}