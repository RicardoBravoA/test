
# Reto Técnico

El proyecto de ejemplo funciona, pero no con las mejores prácticas y patrones de diseño


## Realizar

Esperamos que puedas aplicar todos los conocimientos que tengas sobre:

- Buenas prácticas de programación
- Patrones de diseño
- Manejo de hilos
- Guidelines
- CheckStyle, Linters
- Arquitectura mobile
- Código escalable
- Manejo de errores
- Soporte Android API >= 21
- Pruebas unitarias

Puntos adicionales:
- CI/CD
- Seguridad
- Mínimo uso de dependencias externas
- Persistencia de las peticiones correctas

Debes pensar que el refactor que se realizará se enviará a producción, por ende el código debe ser escalable, mantenible, libre de bugs, etc


## Contacto

Para alguna sugerencia o mejora, escribir a woz@ratkid.org

