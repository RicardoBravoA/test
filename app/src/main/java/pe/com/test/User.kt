package pe.com.test

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("avatar_url")
    val avatar: String?, val name: String?
)
