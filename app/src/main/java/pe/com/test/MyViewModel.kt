package pe.com.test

import android.annotation.SuppressLint
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class MyViewModel(application: Application):AndroidViewModel(application) {

    val data = MutableLiveData<User?>()
    val error = MutableLiveData<String>()
    @SuppressLint("StaticFieldLeak")
    val context = getApplication<Application>().applicationContext

    fun data(value: String) {
        Log.i("z- value", value)
        viewModelScope.launch {
            val response = ApiManager.get().service(value)
            if (response.isSuccessful){
                data.value=response.body()
            } else {
                error.value = context.getString(R.string.errorSearch)
            }
        }
    }

}